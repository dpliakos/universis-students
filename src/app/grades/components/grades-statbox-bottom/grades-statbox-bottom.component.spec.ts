import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GradesStatboxBottomComponent } from './grades-statbox-bottom.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TranslateModule} from '@ngx-translate/core';
import {MostModule} from '@themost/angular';
import {GradesService} from '../../services/grades.service';
import {ConfigurationService, GradeScaleService, SharedModule} from '@universis/common';
import {TestingConfigurationService} from '../../../test';
import {ChartsModule} from 'ng2-charts';
import {NgPipesModule} from 'ngx-pipes';
import {ModalModule, TooltipModule} from 'ngx-bootstrap';

describe('GradesStatboxBottomComponent', () => {
  let component: GradesStatboxBottomComponent;
  let fixture: ComponentFixture<GradesStatboxBottomComponent>;

  const gradeSvc = jasmine.createSpyObj('GradesService', ['getAllGrades', 'getCourseTeachers', 'getGradeInfo',
    'getDefaultGradeScale', 'getThesisInfo', 'getLastExamPeriod', 'getRecentGrades', 'getGradesSimpleAverage', 'getGradesWeightedAverage']);
  const gradeScaleSvc = jasmine.createSpyObj('GradeScaleService', ['getGradeScales', 'getGradeScale']);

  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      declarations: [ GradesStatboxBottomComponent ],
      imports: [ HttpClientTestingModule,
        TranslateModule.forRoot(),
        ChartsModule,
        SharedModule,
        NgPipesModule,
        TooltipModule.forRoot(),
        MostModule.forRoot({
          base: '/',
          options: {
            useMediaTypeExtensions: false
          }
        })],
      providers: [
        {
          provide: GradesService,
          useValue: gradeSvc
        },
        {
          provide: GradeScaleService,
          useValue: gradeScaleSvc
        },
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GradesStatboxBottomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
